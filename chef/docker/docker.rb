docker_service 'default' do
  action [:create, :start]
end

docker_image 'busybox' do
  action :pull
end

docker_container 'an-echo-server' do
  repo 'busybox'
  port ['10.1.0.118:1234:1234/tcp']
  command "nc -ll -p 1234 -e /bin/cat"
end

# Pull latest image
docker_image 'nginx' do
  tag 'latest'
  action :pull
  notifies :redeploy, 'docker_container[my_nginx]'
end

# Run container mapping containers port 80 to the host's port 80
docker_container 'my_nginx' do
  repo 'nginx'
  tag 'latest'
  port ['10.1.0.118:80:80/tcp']
  host_name 'www'
  domain_name 'localhost.localdomain'
  env 'FOO=bar'
  volumes [ '/etc/nginx/conf.d:/etc/nginx/conf.d' ]
end


# specify the udp protocol
docker_container 'an_udp_echo_server' do
  repo 'alpine'
  tag 'latest'
  command 'nc -ll -p 5007 -e /bin/cat'
  port ['10.1.0.118:5007:5007/tcp']
  action :run
end

# multiple ips
docker_container 'multi_ip_port' do
  repo 'alpine'
  tag 'latest'
  command 'nc -ll -p 8301 -e /bin/cat'
  port ['10.1.0.118:8301:8301/tcp']
  action :run
end

#############
# bind mounts
#############

directory '/hostbits' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

file '/hostbits/hello.txt' do
  content 'hello there\n'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end

directory '/more-hostbits' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

file '/more-hostbits/hello.txt' do
  content 'hello there\n'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end

# docker inspect -f "{{ .HostConfig.Binds }}"
docker_container 'bind_mounter' do
  repo 'busybox'
  command 'ls -la /bits /more-bits'
  volumes ['/hostbits:/bits', '/more-hostbits:/more-bits', '/snow', '/winter:/spring:ro', '/summer']
  action :run_if_missing
end

docker_container 'binds_alias' do
  repo 'busybox'
  command 'ls -la /bits /more-bits'
  binds ['/fall:/sun', '/snow', '/winter:/spring:ro', '/summer']
  action :run_if_missing
end


##############
# volumes_from
##############

# build a chef container
directory '/chefbuilder' do
  owner 'root'
  group 'root'
  action :create
end

## file '/chefbuilder/Dockerfile' do
##   content <<-EOF
##   FROM scratch
##   ADD opt /opt
##   EOF
##   action :create
## end

## docker_image 'chef_container' do
##   tag 'latest'
##   source '/chefbuilder'
##   action :build_if_missing
## end
## 
## # create a volume container
## docker_container 'chef_container' do
##   command 'true'
##   volumes '/opt/chef'
##   action :create
## end

#####
# env
#####

file '/env_file1' do
  content <<-EOF
  GOODBYE=TOMPETTY
  1950=2017
  EOF
  action :create
end

file '/env_file2' do
  content <<-EOF
  HELLO=WORLD
  EOF
  action :create
end

docker_container 'env' do
  repo 'alpine'
  env ['PATH=/usr/bin', 'FOO=bar']
  env_file lazy { '/env_file1' }
  command 'env'
  action :run_if_missing
end

docker_container 'env_files' do
  repo 'alpine'
  env_file lazy { ['/env_file1', '/env_file2'] }
  command 'env'
  action :run_if_missing
end

##########
# cmd_test
##########
directory '/cmd_test' do
  action :create
end

file '/cmd_test/Dockerfile' do
  content <<-EOF
  FROM alpine
  CMD [ "/bin/ls", "-la", "/" ]
  EOF
  action :create
end

docker_image 'cmd_test' do
  tag 'latest'
  source '/cmd_test'
  action :build_if_missing
end

docker_container 'cmd_test' do
  action :run_if_missing
end
